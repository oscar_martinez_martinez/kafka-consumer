package com.oscarcode.kafka.springbootkafkaconsumer.listener;

import com.oscarcode.kafka.springbootkafkaconsumer.LogService;
import com.oscarcode.kafka.springbootkafkaconsumer.model.User;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

@Service
public class KafkaConsumer {

    //@KafkaListener(topics="Kafka_Example", groupId="group_id")
    //public void consume(String message) {
      //  System.out.println("Consumer got this message:" + message);
    //}

    @KafkaListener(topics="PaymentToConsumer", groupId="group_id")
    public void consumePayment(String message) {
        //
        CompletableFuture.supplyAsync(() -> {
            LogService logService = new LogService();
            logService.setLogService("Consumer got this message:" + message, "200", "Kafka" );
            return null;
        });
        System.out.println("Consumer got this message:" + message);
    }


    //@KafkaListener(topics="Kafka_Example_json", groupId="group_json", containerFactory="concurrentKafkaListenerContainerFactory")
    //public void consumeJson(User user) {
    //    System.out.println(user + " Consumer has consumed in Json Format");
    //}
}
